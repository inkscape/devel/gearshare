#!/usr/bin/python3

import os
import sys

sys.path.insert(0, os.path.realpath(
    os.path.join(os.path.dirname(__file__), "..")))

class Machine:
    def __init__(self):
        pass

    def foobar(self):
        print("Hello world")


if (__name__ == "__main__"):
    import pprint
    pp = pprint.PrettyPrinter(indent=4)

    machine = Machine()

    pp.pprint(machine)
