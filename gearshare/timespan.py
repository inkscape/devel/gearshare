#!/usr/bin/python3

from datetime import datetime

class Timespan:
    def __init__(self, start: datetime, end: datetime):
        self._start = start
        self._end = end

    def __eq__(self, other: "Timespan") -> bool:
        return (self._start == other._start) and (self._end == other._end)

    def __ne__(self, other: "Timespan") -> bool:
        return (self._start != other._start) or (self._end != other._end)

    def __lt__(self, other: "Timespan") -> bool:
        return self._end < other._start

    def __gt__(self, other: "Timespan") -> bool:
        return self._start > other._end

    def start(self) -> datetime:
        """@return The beginning datetime of this span of time."""
        return self._start

    def end(self) -> datetime:
        """@return The ending datetime of this span of time."""
        return self._end

    def contains(self, x: datetime) -> bool:
        """@return Whether the given @p datetime is within this span of time."""
        if (self._start <= x) and (self._end >= x):
            return True
        return False

    def overlaps(self, other: "Timespan") -> bool:
        """@return Whether this span of time overlaps another span."""
        if self.__eq__(other):
            return True
        elif self._start >= other._start and self._start < other._end:
            return True
        elif self._end <= other._end and self._end > other._start:
            return True
        return False
