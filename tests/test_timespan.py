#!/usr/bin/python3

import sys, os.path
sys.path.insert(0, os.path.realpath(os.path.join(os.path.dirname(__file__), "..")))

import unittest
from datetime import (datetime, timedelta)

from gearshare.timespan import Timespan

class TestTimespan(unittest.TestCase):
    def test_create_timespan(self):
        start = datetime.now()
        end = start + timedelta(days=1)

        timespan = Timespan(start, end)
        self.assertIsNotNone(timespan)
        self.assertEqual(start, timespan.start())
        self.assertEqual(end, timespan.end())

    def test_date_in_timespan(self):
        start = datetime.now()
        end = start + timedelta(days=2)
        before = start - timedelta(days=1)
        after = end + timedelta(days=1)
        timespan = Timespan(start, end)

        middle = start + timedelta(days=1)
        self.assertTrue(timespan.contains(start))
        self.assertTrue(timespan.contains(middle))
        self.assertTrue(timespan.contains(end))

    def test_date_before_timespan(self):
        start = datetime.now()
        end = start + timedelta(days=1)
        timespan = Timespan(start, end)

        before = start - timedelta(days=1)
        self.assertFalse(timespan.contains(before))

    def test_date_after_timespan(self):
        start = datetime.now()
        end = start + timedelta(days=1)
        timespan = Timespan(start, end)

        after = end + timedelta(days=1)
        self.assertFalse(timespan.contains(after))

    def test_timespan_ordering(self):
        start = datetime.now()
        duration = timedelta(days=1)
        timespan_0 = Timespan(start, start + duration)
        timespan_1 = Timespan(start + 2*duration, start + duration)
        timespan_2 = Timespan(start + 4*duration, start + duration)

        self.assertTrue(timespan_0 < timespan_1)
        self.assertTrue(timespan_2 > timespan_1)
        self.assertFalse(timespan_2 < timespan_0)

    def test_timespan_sorting(self):
        start = datetime.now()
        duration = timedelta(days=1)
        timespan_0 = Timespan(start, start + duration)
        timespan_1 = Timespan(start + 2*duration, start + duration)
        timespan_2 = Timespan(start + 4*duration, start + duration)
        timespan_3 = Timespan(start + 6*duration, start + duration)

        timespans_sorted = [ timespan_0, timespan_1, timespan_2, timespan_3 ]
        timespans = [ timespan_2, timespan_1, timespan_3, timespan_0 ]
        timespans.sort(key=lambda timespan: timespan._start)
        self.assertEqual(timespans_sorted, timespans)

    def test_timespan_overlap(self):
        start = datetime.now()
        duration = timedelta(days=1)
        timespan_0 = Timespan(start, start + 2*duration)
        timespan_1 = Timespan(start + duration, start + 2*duration)
        timespan_2 = Timespan(start + 4*duration, start + 6*duration)

        self.assertTrue(timespan_0.overlaps(timespan_1))
        self.assertFalse(timespan_0.overlaps(timespan_2))

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestTimespan))
    return suite

if __name__ == '__main__':
    unittest.main()

