# gearshare

A cloud node "lending library".  Allows teams to parcel out root access to host nodes, with fine grained access control over who can reboot, reimage, upgrade, and gain root access on devel, staging, production, etc. nodes within the collection.